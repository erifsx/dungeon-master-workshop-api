/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devildawgs.dungeonmaster.collectables;

/**
 * Any type of collectable that the player can pick up during his time in the
 * dungeon.
 * 
 * @author Eric
 */
public interface Collectable {
    /**
     * A method that must be implemented to return the
     * human readable name of the collectable.
     * @return A String representation the name of the collectable
     */
    public String getName();
    
    
    /**
     * A method that must be implemented to return the
     * description of the collectable for use in menus
     * or when the item is picked up for the first time.
     * @return A String representation of the description
     */
    public String getDescription();
}
