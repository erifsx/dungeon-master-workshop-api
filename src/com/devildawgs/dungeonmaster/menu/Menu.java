/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devildawgs.dungeonmaster.menu;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *  A simple menu that allows menu items to be added it to control certian
 *  characteristics of the game.  The reason this class is not a singleton
 *  is to allow there to be multiple menus (like having sub menus) that will
 *  be accessable from a main menu.
 *  
 *  @author Eric
 */
public class Menu {
    private ArrayList<MenuItem> menuItems;
    Scanner reader = new Scanner(System.in);
    private static final int maxMenuItems = 5;
    
    
    /**
     * A blank slate menu to add things to.
     */
    public Menu(){
        menuItems = new ArrayList<MenuItem>();
    }
    
    
    
    /**
     * Create a menu with pre-populated menu items
     * 
     * @param menuItems The list of menu items to show
     */
    public Menu(ArrayList<MenuItem> menuItems){
        this.menuItems = menuItems;
    }
    
    
    
    /**
     * Add a menu item to the menu
     * 
     * @param menuItem The menu item to add to the menu
     * @exception Exception thrown for going over the maxMenuItems constant of the Menu class
     */
    public void addMenuItem(MenuActionItem menuItem) throws Exception{
        if(menuItems.size() <= Menu.maxMenuItems)
            menuItems.add(menuItem);
        else
            throw new Exception("Can't add another menu item in this menu," + 
                                " over maxMenuItems constant in Menu class.");
    }
    
    
    /**
     * Shows this menu on the screen and does the command the user selects
     */
    public void show(){
        System.out.println("Please select an option");  // promt the user to select an option
        for(int i = 0; i < menuItems.size(); i++){
            try {
                System.out.println(i+1 + ". " + menuItems.get(i).show());
            } catch (Exception e) {
                System.out.println(e);
                return;
            }
        }
        
        MenuItem selectedItem = menuItems.get(reader.nextInt()-1);
        if(!doItemSelected(selectedItem))
            show();
    }
    
    /**
     * Does the onSelect method of the selected item if, and only if,
     * it's an instance of the MenuActionItem
     * 
     * @param selectedItem The item selected in the menu
     * @return Whether or not it called the onSelect (because of the type of the menu item).
     */
    private boolean doItemSelected(MenuItem selectedItem){
        if(selectedItem instanceof MenuActionItem){
            ((MenuActionItem)selectedItem).onSelect();
            return true;
        }
        return false;
    }
}
