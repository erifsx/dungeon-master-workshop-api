package com.devildawgs.dungeonmaster.menu;

/**
 *  An item to show up in a menu that doesn't perform an action but
 *  does show information to the user.
 * 
 * @author Eric
 */
public abstract class MenuDisplayItem implements MenuItem{
    public static final int maxStringSize = 100;
            
    /**
     * This method returns the menu item text.  This string must be under
     * the maxStringSize constant in the MenuActionItem class.
     * 
     * @return The String to be printed as the menu item.
     */
    protected abstract String getText();
    
    
    /**
     * This method is called by the Menu class to retrieve the string
     * returned in the getText() method, but performs a check to make sure that
     * text isn't over the maxStringSize constant
     * 
     * @return The tested getText() string for the Menu clas
     * s
     * @throws Exception For menu item's text being larger than maxStringSize
     */
    @Override
    public String show() throws Exception{
        String menuText = getText();
        if(menuText.length() < MenuDisplayItem.maxStringSize){
            return menuText;
        }else{
            throw new Exception("Menu show method returns a string longer than maxStringSize constant");
        }
    }
    
}
