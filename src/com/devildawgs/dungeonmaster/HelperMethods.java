/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devildawgs.dungeonmaster;

import com.devildawgs.dungeonmaster.menu.Menu;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  A collection of static methods that may prove useful during development.
 * 
 * @author Eric
 */
public class HelperMethods {
    private static final int numLinesClearScreen = 50;
    private static HelperMethods helperMethods = new HelperMethods();
    
    public static HelperMethods get(){
        return helperMethods;
    }
    /**
     * My really ghetto solution to clear the screen while still being
     * cross platform.  It just prints out a pre-defined number of
     * blank lines...
     */
    public void ghettoClearScreen(){
        for(int i = 0; i < HelperMethods.numLinesClearScreen; i++){
            System.err.println();
        }
    }
    
    
    /**
     * My less ghetto solution to clear the screen, only will work on windows
     * though.  I think it'll only run if done from the command line
     */
    public void clearScreen(){
        try {
            Runtime.getRuntime().exec("cls");
        } catch (IOException ex) {
            Logger.getLogger(HelperMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Pause the current thread for a specified time in milliseconds
     */
    public synchronized void pause(long time){
        try {
            wait(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(HelperMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
