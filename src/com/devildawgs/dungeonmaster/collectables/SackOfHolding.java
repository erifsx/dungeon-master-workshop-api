package com.devildawgs.dungeonmaster.collectables;

/**
 *  This collectable was created to hold large amounts of objects
 * @author Eric
 */
public class SackOfHolding implements Collectable{
    private String name = "Sack of Holding";
    private String description = "A magical sack that is capable of holding immense " +
            "amounts of cargo that would be any traveler's dream to possess.  " +
            "Unfortunately it has a rip in it so it can't hold stuff anymore... " +
            "At least it sounds cool on paper.";
    
    public SackOfHolding(){};

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
    
    
}
