package com.devildawgs.dungeonmaster.menu;

/**
 *  A Menu item to be added to a menu that can perform actions.  
 * 
 *  @author Eric
 */
public abstract class MenuActionItem implements MenuItem{
    public static final int maxStringSize = 100;
            
    /**
     * This method returns the menu item text.  This string must be under
     * the maxStringSize constant in the MenuActionItem class.
     * 
     * @return The String to be printed as the menu item.
     */
    protected abstract String getText();
    
    
    /**
     * This method is called by the Menu class to retrieve the string
     * returned in the getText() method, but performs a check to make sure that
     * text isn't over the maxStringSize constant
     * 
     * @return The tested getText() string for the Menu clas
     * s
     * @throws Exception For menu item's text being larger than maxStringSize
     */
    @Override
    public String show() throws Exception{
        String menuText = getText();
        if(menuText.length() < MenuActionItem.maxStringSize){
            return menuText;
        }else{
            throw new Exception("Menu show method returns a string longer than maxStringSize constant");
        }
    }
    
    
    /**
     * Define the action to do when this menu item is selected.
     */
    public abstract void onSelect();
}
