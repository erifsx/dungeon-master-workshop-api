/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devildawgs.dungeonmaster;

import com.devildawgs.dungeonmaster.collectables.Collectable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *  This is class is a singleton to represent your player.  It should be
 *  instantiated only once per application launch (hence why it's a singleton,
 *  there should only ever be one reference to the object throughout the program).
 * 
 * @author Eric
 */
public class Player {
    private static Player player = new Player();                        // the object holds an instance of itself, so there is only one
    private ArrayList<Collectable> collectables = new ArrayList<Collectable>();    // the list of collectables
    private String playerName = new String();
    private String playerDescription = new String();
    private int turnCount = 0;
    private int playerHealth = 100;
    private HashMap<String, Object> playerProperties = new HashMap<String, Object>();
    private Player(){};
    
    
    
    /**
     * Gets a reference to the player object.  This can be called
     * as many times as nessecary and it will always return the one
     * and only player object.
     */
    public static Player getPlayer(){
        return player;                                                  // return the player object
    }
    
    
    
    /**
     * Get an list of all of the player's collectables to either add, remove
     * or read their collectables
     * 
     * @return ArrayList<Collectable>
     */
    public ArrayList<Collectable> getCollectablesList(){
        return collectables;
    }
    
    
    /**
     * A method that automatically prints out each of the players methods,
     * suguested for use in a menu system.  Please note this method provides
     * no pagation, which may be nessecary for larger lists of collectables.  It's
     * mearly an example to build upon.
     */
    public void printPlayerCollectables(){
        if(collectables.size() > 0){
            System.out.println("Player " + this.getPlayerName() + "'s collectables");
            System.out.println();
            
            for(Collectable c : collectables){
                System.out.println("  " + c.getName());
                System.out.println("  " + c.getDescription());
                System.out.println();
            }
        }else{
            System.out.println("Player " + this.getPlayerName() + " currently has no collectables.");
        }        
    }
    
    
    
    /**
     * Get the player's name
     * 
     * @return String playerName
     */
    public String getPlayerName(){
        return playerName;
    }
    
    
    
    /**
     * Get the player's description (like for the menu).
     * 
     * @return String playerDescription
     */
    public String getPlayerDescription(){
        return playerDescription;
    }
    
    
    /**
     * Get the number of turns the player has gone though since the beginning
     * of the game
     * 
     * @return The number of turns the player has gone though
     */
    public int getPlayerTurns(){
        return this.turnCount;
    }
    
    
    
    /**
     * Gets the player's current health.
     * 
     * @return playerHealth The percentage (from 0 to 100) of the players remaining health
     */
    public int getPlayerHealth(){
        return this.playerHealth;
    }
    
    
    /**
     * Set the player's name for later lookup.
     * 
     * @param playerName the name of the player
     */
    public void setPlayerName(String playerName){
        this.playerName = playerName;
    }
    
     
    /**
     * Set the player's description for later lookup.
     * 
     * @param playerDescription the player's description
     */
    public void setPlayerDescription(String playerDescription){
        this.playerDescription = playerDescription;
    }
    
    
    /**
     * Sets the number of turns the player has gone though
     * 
     * @param turnCount The number of turns the player has gone through 
     */
    public void setPlayerTurns(int turnCount){
        this.turnCount = turnCount;
    }
    
    
    /**
     * Set the player's remaining health.  This number
     * will be checked on entering to make sure it's not
     * less than zero.
     * 
     * @param percent The health the player now has
     */
    public void setPlayerHealth(int percent){
        if(percent >= 0)
            this.playerHealth = percent;
        else
            this.playerHealth = 0;
        
        if(this.playerHealth == 0)
            loseGame("You lost the game because your health hit zero!");
    }
    
    /**
     * Set a property on the player
     * 
     * @param propertyName The name of the property
     * @param property The Object representation of the property to be saved
     */
    public void setPlayerProperty(String propertyName, Object property){
        playerProperties.put(propertyName, property);
    }
    
    /**
     * Get the property that was set on the player
     * 
     * @param propertyName The name of the property to set
     * @return The object representation of the property that was saved with that name
     */
    public Object getPlayerProperty(String propertyName){
        return playerProperties.get(propertyName);
    }
    
    
    /**
     * A simple way of incrementing the number of turns the player has gone
     * through by one
     */
    public void incrementPlayerTurns(){
        this.turnCount++;
    }
    
    
    /**
     * To be called when the player wins the game
     */
    public void winGame(){
        System.out.println("Congradulations you won the game!");
        System.out.println("It took you " + getPlayerTurns() + " turns");
        System.exit(0);
    }
    
    
    /**
     * To be called when the player has lost the game.  Is also called
     * when the player's health reaches 0.
     * 
     * @param message The message to display when the player loses
     */
    public void loseGame(String message){
        System.out.println(message);
        System.exit(0);
    }
}
